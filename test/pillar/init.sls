# See Oracle Java SE checksums page here: https://www.oracle.com/webfolder/s/digest/8u152checksum.html
java:
  prefix: /usr/share/java
  java_symlink: /usr/bin/java
  javac_symlink: /usr/bin/javac
  dl_opts: -b oraclelicense=accept-securebackup-cookie -L -s
  archive_type: tar
  # Enable alternatives feature by setting nonzero 'alt_priority' value here.
  # Increase same value on each subsequent software installation.
  # alt_priority: 301800111
  ## JDK ##
  version_name: jdk1.8.0_162
  source_url: http://download.oracle.com/otn-pub/java/jdk/8u162-b12/0da788060d494f5095bf8624735fa2f1/jdk-8u162-linux-x64.tar.gz
  source_hash: sha256=68ec82d47fd9c2b8eb84225b6db398a72008285fafc98631b1ff8d2229680257
  jre_lib_sec: /usr/share/java/jdk1.8.0_162/jre/lib/security
  java_real_home: /usr/share/java/jdk1.8.0_162
  java_realcmd: /usr/share/java/jdk1.8.0_162/bin/java
  javac_realcmd: /usr/share/java/jdk1.8.0_162/bin/javac
  ## or JRE ##
  # version_name: jre1.8.0_152
  # source_url: http://download.oracle.com/otn-pub/java/jdk/8u152-b16/aa0333dd3019491ca4f6ddbe78cdb6d0/jre-8u152-linux-x64.tar.gz
  # source_hash: sha256=e75668f82c51ddd1ab6dd34c69cdb21644c7f9fa887640cdfb5d002e908d9e32
  # jre_lib_sec: /usr/share/java/jre1.8.0_152/jre/lib/security
  # java_real_home: /usr/share/java/jre1.8.0_152
  # java_realcmd: /usr/share/java/jre1.8.0_152/bin/java
  # javac_realcmd:
  ## and JCE ##
  jce_url: http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip
  jce_hash: sha256=f3020a3922efd6626c2fff45695d527f34a8020e938a49292561f18ad1320b59
# java:version_name is the name of the top-level directory inside the tarball
# java:prefix is where the tarball is unpacked into - prefix/version_name being
#             the location of the jdk or jre
  certificates:
    /srv/certs/intermediateca.pem:
    /srv/certs/rootca.pem:
