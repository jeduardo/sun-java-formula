include:
  - sun-java
  - base: pki
  - base: pki.local

{%- from 'sun-java/settings.sls' import java with context %}

{%- for certificate, properties in salt['pillar.get']('java:certificates', {}).items() %}

add {{ certificate }} to trust store:
  cmd.run:
    - name: |
        export CN="$(openssl x509 -in {{ certificate }} -subject -noout -nameopt multiline | sed -n 's/ *commonName *= //p' | tr '[:upper:]' '[:lower:]')"
        export TIME="$(date -d "$(openssl x509 -in {{ certificate }}  -noout -enddate | sed 's/notAfter=//')" +'%Y-%m-%d %H:%M:%S')" 
        export ALIAS="$CN $TIME"
        {{ java.java_real_home }}/bin/keytool -keystore {{ java.jre_lib_sec }}/cacerts -storepass changeit -importcert -noprompt -alias "$ALIAS" -file {{ certificate }}
    - unless: |
        export CN="$(openssl x509 -in {{ certificate }} -subject -noout -nameopt multiline | sed -n 's/ *commonName *= //p' | tr '[:upper:]' '[:lower:]')"
        export TIME="$(date -d "$(openssl x509 -in {{ certificate }}  -noout -enddate | sed 's/notAfter=//')" +'%Y-%m-%d %H:%M:%S')"
        export ALIAS="$CN $TIME"
        {{ java.java_real_home }}/bin/keytool -keystore {{ java.jre_lib_sec }}/cacerts -storepass changeit -list | grep "$ALIAS"

{%- endfor %}
